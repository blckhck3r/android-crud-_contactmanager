package acinerbanujla.simplecm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;


public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASENAME = "Contact.db";
    private static final String TABLENAME = "contact_tbl";
    private static final String ID = "ID";
    private static final String FIRSTNAME = "fname";
    private static final String LASTNAME = "lname";
    private static final String EMAIL = "cmail";
    private static final String NUMBER = "cnumber";

    public DatabaseHelper(Context context) {
        super(context, DATABASENAME, null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLENAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                "fname VARCHAR(255) NOT NULL," +
                "lname VARCHAR(255) NOT NULL," +
                "cmail VARCHAR(255) NOT NULL," +
                "cnumber VARCHAR(255) NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLENAME);
        onCreate(db);
    }

    public boolean insertData(String fname, String lname, String cmail, String cnumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put(FIRSTNAME, fname);
            contentValues.put(LASTNAME, lname);
            contentValues.put(EMAIL, cmail);
            contentValues.put(NUMBER, cnumber);

            long result = db.insert(TABLENAME, null, contentValues);
            if (result > 0) {
                db.setTransactionSuccessful();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        Cursor rs = db.rawQuery("SELECT * FROM " + TABLENAME, null);
        db.setTransactionSuccessful();
        db.endTransaction();
        return rs;
    }

    public Integer deleteData(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        db.endTransaction();
        return db.delete(TABLENAME, "ID = ?", new String[]{id});
    }

    public boolean updateData(String id, String fname, String lname, String cmail, String cnumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        db.beginTransaction();
        try {
            contentValues.put(ID, id);
            contentValues.put(FIRSTNAME, fname);
            contentValues.put(LASTNAME, lname);
            contentValues.put(EMAIL, cmail);
            contentValues.put(NUMBER, cnumber);
            long result = db.update(TABLENAME, contentValues, "ID = ?", new String[]{id});
            if (result > 0) {
                db.setTransactionSuccessful();
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        } finally {
            db.endTransaction();
            db.close();
        }

    }

}
