package acinerbanujla.simplecm;

import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import android.content.DialogInterface;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DatabaseHelper dbConn;
    EditText firstName, lastName, contactEmail, contactNumber, searchfield;
    Button btnInsert, btnView, btnDelete, btnUpdate, exitBtn;
    Dialog myDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dbConn = new DatabaseHelper(this);
        super.onCreate(savedInstanceState);
//       supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        setView();

    }

    public void setView() {
        firstName = (EditText) findViewById(R.id.fname);
        lastName = (EditText) findViewById(R.id.lname);
        contactEmail = (EditText) findViewById(R.id.cmail);
        contactNumber = (EditText) findViewById(R.id.cnumber);
        btnInsert = (Button) findViewById(R.id.add_btn);
        btnView = (Button) findViewById(R.id.view_btn);
        searchfield = (EditText) findViewById(R.id.id_search);
        btnUpdate = (Button) findViewById(R.id.update_btn);
        exitBtn = (Button) findViewById(R.id.exit_btn);
        addData();
        viewData();
        delData();
        updateData();
        exitBtn();

    }

    public void MyCustomDialog(){
        Toast.makeText(getApplicationContext(),"\t          Simple Contact Manager\nSource Code: bitbucket.com/blckhck3r",
                Toast.LENGTH_LONG).show();
        myDialog = new Dialog(MainActivity.this);
        myDialog.setContentView(R.layout.custom_dialog);
        myDialog.setTitle("About");
        myDialog.show();



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.about:
                MyCustomDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_menu,menu);
        return true;
    }

    public void exitBtn() {
        exitBtn = (Button) findViewById(R.id.exit_btn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Thank you for using this app.", Toast.LENGTH_LONG).show();
                popupDialog();
            }
        });

        exitBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(getApplicationContext(), "This program is created by:\n          Abrenica, Aljun", Toast.LENGTH_LONG).show();
                popupDialog();
                return true;
            }
        });
    }

    public void popupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to close this application? ").setCancelable(false).setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast("Exitting. . .");
                        finish();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbConn.close();
    }

    public void updateData() {
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String fn = firstName.getText().toString().trim();
                String ln = lastName.getText().toString().trim();
                String ce = contactEmail.getText().toString().trim();
                String cn = contactNumber.getText().toString().trim();
                if (searchfield.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "              Fill in the search field,\nBefore proceeding this update button.", Toast.LENGTH_SHORT).show();
                    return;
                } else if (fn.length() == 0) {
                    Toast("Fill the firstname text field.");
                    return;
                } else if (ln.length() == 0) {
                    Toast("Fill the lastname text field.");
                    return;
                } else if (ce.length() == 0) {
                    Toast("Fill the email text field.");
                    return;
                } else if (cn.length() == 0) {
                    Toast("Fill the contact number field.");
                    return;
                } else {
                    boolean isUpdate = dbConn.updateData(searchfield.getText().toString(), fn, ln, ce, cn);
                    if (isUpdate) {
                        Toast.makeText(getApplicationContext(), "Successfully Updated", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Data not updated", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                clearText();
            }
        });
    }
    public void Toast(String s){
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
    }
    public void delData() {
        btnDelete = (Button) findViewById(R.id.del_btn);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchfield.getText().length() == 0) {
                    Toast.makeText(getApplicationContext(), "            Fill in the search field,\nBefore proceeding this delete button.", Toast.LENGTH_SHORT).show();
                    return;
                }
                Integer deleteRows = dbConn.deleteData(searchfield.getText().toString());
                if (deleteRows > 0) {
                    Toast.makeText(getApplicationContext(), "Successfully Deleted", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Data not deleted", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void addData() {
        btnInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateInsert();
            }
        });
    }

    public void validateInsert() {
        if (firstName.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the firstname text field", Toast.LENGTH_SHORT).show();
            return;
        } else if (lastName.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the lastname text field", Toast.LENGTH_SHORT).show();
            return;
        } else if (contactEmail.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the email text field", Toast.LENGTH_SHORT).show();
            return;
        } else if (contactNumber.getText().length() == 0) {
            Toast.makeText(getApplicationContext(), "Fill in the contact Number text field", Toast.LENGTH_SHORT).show();
            return;
        } else {
            boolean isInserted = dbConn.insertData(firstName.getText().toString(), lastName.getText().toString(), contactEmail.getText().toString(), contactNumber.getText().toString());
            if (isInserted == true) {
                Toast.makeText(getApplicationContext(), "Succesfully Inserted", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Data not inserted", Toast.LENGTH_SHORT).show();
            }
            clearText();
        }
    }

    public void clearText() {
        searchfield.setText("");
        firstName.setText("");
        lastName.setText("");
        contactEmail.setText("");
        contactNumber.setText("");
        dbConn.close();
    }

    public void viewData() {
        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor rs = dbConn.getAllData();
                if (rs.getCount() == 0) {
                    //message
                    showMessage("Error", "Contact not found");
                    return;
                }
                StringBuffer buffer = new StringBuffer();
                while (rs.moveToNext()) {
                    buffer.append(" Id: " + rs.getString(0));
                    buffer.append("\n Firstname: " + rs.getString(1));
                    buffer.append("\n Lastname: " + rs.getString(2));
                    buffer.append("\n Email: " + rs.getString(3));
                    buffer.append("\n Number: " + rs.getString(4) + "\n\n");
                }
                showMessage("Contact List\n", buffer.toString());
            }
        });
    }//eof viewData

    public void showMessage(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}
